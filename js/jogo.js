const criaJogo = sprite => {
    let palavraSecreta = '';
    let etapa = 1;
    let lacunas = [];
    
    const setLacunas = () => lacunas = Array(palavraSecreta.length).fill('');
    
    const proximaEtapa = () => etapa++;
    
    const setPalavraSecreta = palavra => {
        if(!palavra.trim()){
            throw Error('Palavra secreta inválida!');
        }
        
        palavraSecreta = palavra;
        
        setLacunas();
        proximaEtapa();
    };
    
    const getLacunas = () => lacunas;
    
    const getEtapa = () => etapa;
    
    const processaChute = chute => {
        if(!chute.trim()){
            throw Error('Chute inválido!');
        }

        const exp = new RegExp(chute, 'gi');
        let acertou = false;
        let resultado;

        while(resultado = exp.exec(palavraSecreta)){
            acertou = true;
            lacunas[resultado.index] = chute;
        }

        if(!acertou){
            sprite.nextFrame();
        }
    };

    const ganhou = () => (lacunas.length > 0) ? !lacunas.some(lacuna => lacuna == '') : false;

    const perdeu = () => sprite.isFinished();

    const ganhouOuPerdeu = () => ganhou() || perdeu();

    const reinicia = () => {
        palavraSecreta = '';
        etapa = 1;
        lacunas = [];
        sprite.reset();
    };

    return {
        setPalavraSecreta,
        getLacunas,
        getEtapa,
        processaChute,
        ganhou,
        perdeu,
        ganhouOuPerdeu, 
        reinicia
    }
}