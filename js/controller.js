const criaController = jogo => {
    const $entrada = $('#entrada');
    const $lacunas = $('.lacunas');

    const exibeLacunas = () => {
        $lacunas.empty();
        jogo.getLacunas().forEach(lacuna => {
            const li = $('<li>').addClass('lacuna').text(lacuna);
            li.addClass('lacuna');
            li.text(lacuna);
            li.appendTo($lacunas);
        });
    };

    const mudaPlaceHolder = texto => $entrada.attr('placeholder', texto);

    const guardaPalavraSecreta = () => {
        try{
            jogo.setPalavraSecreta($entrada.val().trim());
            $entrada.val('');
            mudaPlaceHolder('Chute');
            exibeLacunas();
        } catch(erro){
            alert(erro.message);
        }
    };

    const reinicia = () => {
        jogo.reinicia();
        $lacunas.empty();
        mudaPlaceHolder('Palavra secreta');
    }

    const leChute = () => {
        try{
            jogo.processaChute($entrada.val().trim().substr(0, 1));
            $entrada.val('');
            exibeLacunas();
    
            if(jogo.ganhouOuPerdeu()){
                setTimeout(() => {
                    if(jogo.ganhou()){
                        alert('Ganhou!');
                    }
                    else if(jogo.perdeu()){
                        alert('Perdeu!');
                    }
        
                    reinicia();
                }, 200);
            }
        } catch(erro){
            alert(erro.message);
        }
    };

    const inicia = () => {
        $entrada.keypress(event => {
            if(event.which == 13){
                switch(jogo.getEtapa()){
                    case 1:
                        guardaPalavraSecreta();
                        break;
                    case 2:
                        leChute();
                        break;
                }
            }
        });
    };

    return {
        inicia
    }
}